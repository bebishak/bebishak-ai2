from flask import Flask
import flask.scaffold
flask.helpers._endpoint_from_view_func = flask.scaffold._endpoint_from_view_func
from flask_restx import Api, Resource
from flask_cors import CORS

from werkzeug.utils import secure_filename
from werkzeug.datastructures import FileStorage

import tensorflow as tf
from object_detection.utils import config_util

from object_detection.utils import label_map_util
from object_detection.builders import model_builder

from PIL import Image
import numpy as np

import time

import warnings
warnings.filterwarnings('ignore')   # Suppress Matplotlib warnings

import os

### HTTP SERVER CONFIGURATIONS
app = Flask(__name__)
app.config['UPLOAD_PATH'] = '/input'
app.config['MAX_CONTENT_LENGTH'] = 75 * 1024 * 1024

CORS(app)

ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg'}

api = Api(
    app, version = "1.0.0", 
    title= "Bebishak AI Documentation",
    description = "Submit image and detect ingredients",
)

upload_parser = api.parser()
upload_parser.add_argument(
    'file', location='files',
    type=FileStorage, required=True
)

ai_name_space = api.namespace('detect', descriptions='Manage API for Ingredients Detection AI')

@ai_name_space.route("")
@api.expect(upload_parser)
class Detect(Resource):
    def post(self):
        # Get sent file and save it
        args = upload_parser.parse_args()
        file = args['file']
        file_name = secure_filename(file.filename)
        basepath = os.path.dirname(__file__)

        # # Check if path exists, if not create it
        filepath = os.path.join(basepath, 'input')
        if not os.path.exists(filepath) :
            os.makedirs(filepath)
        
        IMAGE_PATH = os.path.join(
            filepath, file_name
        )
        
        file.save(IMAGE_PATH)

        os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'    # Suppress TensorFlow logging (1)
        

        tf.get_logger().setLevel('ERROR')           # Suppress TensorFlow logging (2)

        # Enable GPU dynamic memory allocation
        gpus = tf.config.experimental.list_physical_devices('GPU')
        for gpu in gpus:
            tf.config.experimental.set_memory_growth(gpu, True)

        IMAGE_PATHS = []
        IMAGE_PATHS.append(IMAGE_PATH)
        
        PATH_TO_LABELS = 'workspace/annotations/label_map.pbtxt'

        PATH_TO_SAVED_MODEL = 'workspace/models/ssd_mobnet_tuned2/export/saved_model/1'

        print('Loading model...', end='')
        start_time = time.time()

        # Load saved model and build the detection function
        detect_fn = tf.saved_model.load(PATH_TO_SAVED_MODEL)

        end_time = time.time()
        elapsed_time = end_time - start_time
        print('Done! Took {} seconds'.format(elapsed_time))

        category_index = label_map_util.create_category_index_from_labelmap(PATH_TO_LABELS,
                                                                            use_display_name=True)

        def load_image_into_numpy_array(path):
            return np.asarray(np.array(Image.open(image_path)))


        for image_path in IMAGE_PATHS:

            print('Running inference for {}... '.format(image_path), end='')

            image_np = load_image_into_numpy_array(image_path)
            input_tensor = tf.convert_to_tensor(image_np)
            input_tensor = input_tensor[tf.newaxis, ...]
            input_tensor = input_tensor[:, :, :, :3]
            # input_tensor = np.expand_dims(image_np, 0)
            detections = detect_fn(input_tensor)

            num_detections = int(detections.pop('num_detections'))
            detections = {key: value[0, :num_detections].numpy()
                        for key, value in detections.items()}
            detections['num_detections'] = num_detections

            # detection_classes should be ints.
            detections['detection_classes'] = detections['detection_classes'].astype(np.int64)

            ingredients = []
                    
            classes = detections['detection_classes']
            for i in range(len(classes)):
                if detections['detection_scores'][i] > 0.5:
                    ingredients.append(category_index.get(classes[i])['name'])
            
            # print(ingredients)
            # print('Done')
        
        # Remove image
        os.remove("input/{file_name}".format(file_name=file_name))
        
        return {
            "status" : "Success",
            "statusCode" : "200",
            "result" : ingredients
        }
        
        #except Exception as e :
        #    ai_name_space.abort(400, status = " Could not retrieve information", statusCode = '400')


if __name__ == '__main__':
    app.run(host="0.0.0.0",threaded=True, port=5000) # we can u
