# FROM tensorflow/serving

# RUN apt-get -y update
# # RUN apt-get -y upgrade
# # RUN apt-get -y install curl

# ENV MODEL_NAME=saved_model
# ENV PORT=8501

# EXPOSE 8501

# COPY ./workspace/models/ssd_mobnet_tuned2/export/saved_model/ ./models/saved_model/

FROM python:3.10

RUN apt-get -y update
RUN apt-get -y upgrade
RUN apt-get -y install git
RUN apt-get -y install curl
RUN apt-get -y install zip unzip libssl-dev

RUN pip install numpy==1.21.5
RUN pip install flask==2.0.2
RUN pip install flask-restx==0.3.0
RUN pip install flask-cors==3.0.10
RUN pip install tensorflow==2.8.0
RUN pip install object-detection==0.0.3
RUN pip install werkzeug==2.0.2
RUN pip install pycocotools==2.0.4
RUN pip install tf_slim==1.1.0
RUN pip install tensorflow-io==0.24.0
RUN pip install tf-models-official==2.8.0
RUN pip install scipy

EXPOSE 5000

COPY . .

RUN mkdir tensorflow
RUN cd tensorflow
RUN git clone --depth 1 https://github.com/tensorflow/models
RUN cd ..
RUN mv models /tensorflow/models

RUN curl -OL "https://github.com/google/protobuf/releases/download/v3.0.0/protoc-3.0.0-linux-x86_64.zip" && \
    unzip protoc-3.0.0-linux-x86_64.zip -d proto3 && \
    mv proto3/bin/* /usr/local/bin && \
    mv proto3/include/* /usr/local/include && \
    rm -rf proto3 protoc-3.0.0-linux-x86_64.zip

RUN cd /tensorflow/models/research && \
    protoc object_detection/protos/*.proto --python_out=.

ENV PYTHONPATH $PYTHONPATH:/tensorflow/models/research:/tensorflow/models/research/slim

CMD ["app.py"]
ENTRYPOINT ["python3"]